//
//  City.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import SwiftyJSON

class City {
    var name: String?
    var description: String?
    var humidity: Int?
    var windSpeed: Int?
    var weatherCode: Int?
    var country: String?
    var medTempeture: Int?
    var minTempeture: Int?
    var maxTempeture: Int?
    var atm: Int?
    
    init(data: JSON, countryName: String) {
        name = data[Constants.ApiKeys.name].string
        let weatherData = getWeatherData(data: data)
        description = weatherData.0
        weatherCode = weatherData.1
        humidity = data[Constants.ApiKeys.main][Constants.ApiKeys.humidity].int
        windSpeed = data[Constants.ApiKeys.wind].dictionary?[Constants.ApiKeys.speed]?.int
        country = countryName
        minTempeture = data[Constants.ApiKeys.main][Constants.ApiKeys.tempMin].int
        maxTempeture = data[Constants.ApiKeys.main][Constants.ApiKeys.tempMax].int
        atm = data[Constants.ApiKeys.main][Constants.ApiKeys.atm].int
        guard minTempeture != nil && maxTempeture != nil else {  return }
        medTempeture = (minTempeture! + maxTempeture!)/2
    }
    
    private func getWeatherData(data:JSON) -> (String?, Int?) {
        var description: String?
        var iconIdentifier: Int?
        if let weathers = data[Constants.ApiKeys.weather].array {
            for weather in weathers {
                description = weather[Constants.ApiKeys.description].string
                iconIdentifier = weather[Constants.ApiKeys.weatherId].int
                break
            }
        }
        return (description, iconIdentifier)
    }
}
