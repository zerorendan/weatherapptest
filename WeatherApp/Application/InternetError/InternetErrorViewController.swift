//
//  InternetErrorViewController.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class InternetErrorViewController: UIViewController {
    //MARK: - Attributes
    var delegate: InternetErrorViewControllerDelegate?
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //MARK: - Logic
    @IBAction func didRetry(_ sender: UIButton) {
        if isConnectedToNetwork() {
            delegate?.didReload()
            dismiss(animated: true, completion: nil)
        }
    }
}
