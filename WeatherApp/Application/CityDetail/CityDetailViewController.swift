//
//  CityDetailViewController.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class CityDetailViewController: UIViewController {
    //MARK: - Attributes
    @IBOutlet var weatherIcon: UIImageView!
    @IBOutlet var tempLabel: UILabel!
    @IBOutlet var weatherDesc: UILabel!
    @IBOutlet var minTemp: UILabel!
    @IBOutlet var medTemp: UILabel!
    @IBOutlet var maxTemp: UILabel!
    @IBOutlet var hum: UILabel!
    @IBOutlet var atm: UILabel!
    @IBOutlet var wind: UILabel!
    var city: City?
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tempLabel.text = "\(city?.medTempeture ?? 0)º"
        weatherIcon.iconFactory(iconId: (city?.weatherCode)!)
        title = city?.name
        weatherDesc.text = city?.description
        minTemp.text = "\(city?.minTempeture ?? 0)º"
        medTemp.text = "\(city?.medTempeture ?? 0)º"
        maxTemp.text = "\(city?.maxTempeture ?? 0)º"
        hum.text = "\(city?.humidity ?? 0)%"
        atm.text = "\(city?.medTempeture ?? 0)º"
        wind.text = "\(city?.windSpeed ?? 0)"
    }
}
