//
//  CityTableViewCell.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {
    
    //MARK: - Attributes
    @IBOutlet var cityName: UILabel!
    @IBOutlet var countryName: UILabel!
    @IBOutlet var weatherIcon: UIImageView!
    
    //MARK: - Logic
    func configure(_ city: City) {
        changeBackground(color: .lightBlue)
        cityName.text = city.name
        countryName.text = city.country
        weatherIcon.iconFactory(iconId: city.weatherCode ?? 0)
    }
    
    func changeBackground(color: UIColor) {
        let cellSelectedBackGround = UIView()
        cellSelectedBackGround.backgroundColor = color
        selectedBackgroundView = cellSelectedBackGround
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setSelectedStateCell(isSelected: selected)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        setSelectedStateCell(isSelected: highlighted)
    }
    
    private func setSelectedStateCell(isSelected:Bool) {
        cityName.textColor = isSelected ? .white : .black
        countryName.textColor = isSelected ? .white : .lightBlue
        selectedBackgroundView?.backgroundColor = isSelected ? .lightBlue : .white
        weatherIcon.tintColor = isSelected ? .white : .lightBlue
    }
}
