//
//  CitiesTableViewController.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import CoreLocation

class CitiesTableViewController: UITableViewController {
    //MARK: - Attributes
    fileprivate var data:[City] = []
    fileprivate var connectionManager = ConnectionManager.shared
    fileprivate let geoCoder = CLGeocoder()
    fileprivate let locationManager = CLLocationManager()
    fileprivate var lastLocationData: [String: String]?
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupGeolocation()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            (segue.destination as! CityDetailViewController).city = sender as? City
        } else if segue.identifier == "showError" {
            (segue.destination as! InternetErrorViewController).delegate = self
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier, for: indexPath) as! CityTableViewCell
        cell.configure(data[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard data.count > 0 else { return }
        performSegue(withIdentifier: "showDetails", sender: data[indexPath.row])
    }
    
    //MARK: - Logic
    @objc func getLocation(_ notification: NSNotification) {
        guard notification.object != nil else {return}
        if let locationData = notification.object as? [String: String] {
            lastLocationData = locationData
            loadCities(locationData: locationData)
        }
    }
    
    private func loadCities(locationData: [String: String]) {
        connectionManager.getCities(latitude: locationData[Constants.LocationDataKeys.latitude]!,
                                    longitude: locationData[Constants.LocationDataKeys.longitude]!,
                                    country: locationData[Constants.LocationDataKeys.country]!,
                                    onSuccess: { cities in
                                        self.data = cities
                                        self.tableView.reloadData()
                                        self.hideLoader()
                                        self.lastLocationData = nil
                                        self.refreshControl?.endRefreshing()
        }, onError: { error in
            print(error ?? Constants.defaultErrorMessage)
            self.showError()
        })
    }
    
    private func setup() {
        navigationController?.navigationBar.prefersLargeTitles = true
        title = Constants.tableTitle
        refreshControl = UIRefreshControl()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.getLocation(_:)),
                                               name: NSNotification.Name(rawValue: Constants.notificationNameLocation),
                                               object: nil)
    }
    
    private func setupGeolocation() {
        locationManager.delegate = self
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            showPermissionMessage()
        case .authorizedAlways,
             .authorizedWhenInUse:
            showloader()
            locationManager.startMonitoringSignificantLocationChanges()
        default:
            print(Constants.defaultErrorMessage)
            showPermissionMessage()
        }
        self.refreshControl?.addTarget(self, action: #selector(self.checkLocation), for: .valueChanged)
    }
    
    
    @objc private func checkLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
        }
    }
    
    private func showPermissionMessage() {
        let alert = UIAlertController(title: "Important",
                                      message: "Please enable location permissions.\nThis app will not work without them",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.startMonitoringSignificantLocationChanges()
        }))
        present(alert, animated: false, completion: nil)
    }
    
    private func showError() {
        performSegue(withIdentifier: "showError", sender: nil)
    }
}

//MARK: - CLLocationManagerDelegate
extension CitiesTableViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.first else { return }
        geoCoder.reverseGeocodeLocation(currentLocation) { (placemarks, _) in
            guard let currentLocationPlacemark = placemarks?.first else { return }
            var locationData:[String:String] = [:]
            locationData[Constants.LocationDataKeys.latitude] = "\(currentLocation.coordinate.latitude)"
            locationData[Constants.LocationDataKeys.longitude] = "\(currentLocation.coordinate.longitude)"
            locationData[Constants.LocationDataKeys.country] = "\(currentLocationPlacemark.country ?? Constants.defaultErrorText)"
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.notificationNameLocation), object: locationData)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
        refreshControl?.endRefreshing()
    }
}

//MARK: - InternetErrorViewControllerDelegate
extension CitiesTableViewController: InternetErrorViewControllerDelegate {
    func didReload() {
        guard lastLocationData != nil else { return }
        loadCities(locationData: lastLocationData!)
    }
}
