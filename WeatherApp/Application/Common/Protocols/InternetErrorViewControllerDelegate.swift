//
//  InternetErrorViewControllerDelegate.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

protocol InternetErrorViewControllerDelegate {
    func didReload()
}
