//
//  Constants.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import Foundation
struct Constants {
    static let connectionTimeout:TimeInterval = 45
    static let apiKey = "558c8270089086a25bded0e8be535652"
    static let urlCitiesInCycle = "http://api.openweathermap.org/data/2.5/find?lat=%@&lon=%@&cnt=10&appid=%@&units=metric"
    static let cellIdentifier = "cityCell"
    static let tableTitle = "Weather"
    static let nibCellName = "CityTableViewCell"
    static let notificationNameLocation = "locationsUpdate"
    static let defaultErrorText =  "UNKNOWN"
    static let defaultErrorMessage =  "Something went wrong"
    static let emptyString = ""
    
    struct LocationDataKeys {
        static let latitude = "latitude"
        static let longitude = "longitude"
        static let country = "country"
    }
    
    struct ApiKeys {
        static let code = "cod"
        static let list = "list"
        static let name = "name"
        static let weather = "weather"
        static let description = "description"
        static let main = "main"
        static let humidity = "humidity"
        static let wind = "wind"
        static let speed = "speed"
        static let tempMin = "temp_min"
        static let tempMax = "temp_max"
        static let atm = "temp"
        static let weatherId = "id"
    }
}
