//
//  Color.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

extension UIColor {
    static let lightBlue = UIColor(red: 25.0 / 255.0, green: 123.0 / 255.0, blue: 214.0 / 255.0, alpha: 1.0)
}
