//
//  ViewController.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import MBProgressHUD
import Reachability

extension UIViewController {
    func showloader() {
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    
    func hideLoader() {
        MBProgressHUD.hide(for: view, animated: true)
    }
    
    func isConnectedToNetwork() -> Bool {
        do {
            let reachability = Reachability()!
            
            switch reachability.connection {
            case .wifi:
                return true
            case .cellular:
                return true
            case .none:
                return false
            }
        }
    }
}
