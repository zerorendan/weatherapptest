//
//  ImageView.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
extension UIImageView {
    func iconFactory(iconId: Int) {
        if iconId >= 200 && iconId <= 531 {
            image = #imageLiteral(resourceName: "rainy")
        } else if iconId >= 800 && iconId <= 804 {
            image = iconId == 800 ? #imageLiteral(resourceName: "sunny") : #imageLiteral(resourceName: "sun - cloudy")
        } else {
            image = #imageLiteral(resourceName: "menu_weather")
        }
    }
}
