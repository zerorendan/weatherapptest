//
//  ConnectionManager.swift
//  WeatherApp
//
//  Created by Juan Calvo on 9/5/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ConnectionManager {
    // MARK: - Properties
    static let shared = ConnectionManager()
    private let sessionManager: SessionManager
    private let utilityQueue: DispatchQueue
    private let statusOk = "200"
    
    // MARK: - Init
    private init() {
        let extendedTimeoutConfiguration = URLSessionConfiguration.default
        extendedTimeoutConfiguration.timeoutIntervalForRequest = Constants.connectionTimeout
        extendedTimeoutConfiguration.timeoutIntervalForResource = Constants.connectionTimeout
        sessionManager = Alamofire.SessionManager(configuration: extendedTimeoutConfiguration)
        utilityQueue = DispatchQueue.global(qos: .utility)
    }
    
    func getCities(latitude:String, longitude: String, country: String, onSuccess
        successBlock: (([City]) -> Void)!,
                   onError errorBlock: ((String?) -> Void)!) {
        let newUrl = String(format: Constants.urlCitiesInCycle, latitude, longitude, Constants.apiKey) as String
        sessionManager.request(URL(string: newUrl)!,
                               method: .get,
                               parameters: nil,
                               encoding: JSONEncoding.default,
                               headers: nil).responseJSON { response in
                                switch response.result {
                                case .success(let data):
                                    let apiData = JSON(data)
                                    if apiData[Constants.ApiKeys.code].string == self.statusOk {
                                        let citiesData = apiData[Constants.ApiKeys.list].array
                                        var cities:[City] = []
                                        cities = (citiesData?.compactMap({ City(data: $0, countryName: country)}))!
                                        successBlock(cities)
                                    } else {
                                        errorBlock("\(Constants.defaultErrorMessage)\(apiData[Constants.ApiKeys.code].string ?? Constants.defaultErrorText)")
                                    }
                                case .failure(let error):
                                    errorBlock(error.localizedDescription)
                                }
        }
    }
}
